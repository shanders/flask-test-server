
initialColor = "#080820";
dragonColor  = "#200808";

document.getElementById('bod').onscroll = () => {
  var doc = document.documentElement;
  var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
  var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

  if(top < 100) {
    document.getElementById('bod').style.backgroundColor = initialColor;
  } else if(top < 200) {
    document.getElementById('bod').style.backgroundColor = dragonColor;
  }
};
