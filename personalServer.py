from flask import Flask, jsonify, request
import requests, json
from datetime import datetime

app = Flask(__name__)
# use Ctrl+Shift+R to force chrome to uncache
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
url="http://0.0.0.0:5000/"
msg="Hello World!"

@app.route('/')
def index():
    print("routed to /")
    return app.send_static_file('personal.html')

@app.route('/personalstyles')
def perstyles():
    return app.send_static_file('personal.css')

@app.route('/message/<msg>', methods=['GET'])
def message(msg):
    return 'wow, so cool that you said ' + msg

@app.route('/data/get')
def getData():
    startdate = request.args.get('start', default = getTodayStr(),      type = str)
    enddate   = request.args.get('end',   default = get30DayDeltaStr(), type = str)
    return jsonify(collectLines(startdate, enddate))

@app.route('/data/put')
def putData():
    date = request.args.get('date', default = "ERROR") + ","
    starttime = request.args.get('start', default = "ERROR") + ","
    endtime   = request.args.get('end',   default = "ERROR") + ","
    name = request.args.get("name", default = "ERROR") + ","
    desc = request.args.get("desc", default = "") + ","
    tags = request.args.get("tags", default = "ERROR") + ","
    whoelse = request.args.get("whoelse", default = "")
    full = date+starttime+endtime+name+desc+tags+whoelse
    if "ERROR" in full:
        print("There was an error inserting the datapoint: " + full)
        return "ERROR"
    with open("static/datapoints.csv", "a") as fp:
        fp.write(full)
    return "SUCCESS"

if __name__=='__main__':
    print("Running server at ", url)
    app.run(host="0.0.0.0", port=5000, debug=True)
